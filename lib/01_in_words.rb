class Fixnum
  def in_words
    str = self.to_s
    i = 0
    arr =
      if str.length % 3 == 0
        str.chars.each_slice(3).to_a
      else
        str.chars.reverse.each_slice(3).to_a.reverse.map do |arr|
          arr.reverse
        end
      end
    arr = arr.map {|arr| arr.join}
    final_res = []
    arr.reverse.each.with_index do |set, idx|
      if idx == 0
        final_res << convert_three_digit(set)
      elsif idx == 1
        final_res << "thousand" unless set.to_i == 0
        final_res << convert_three_digit(set)
      elsif idx == 2
        final_res << "million" unless set.to_i == 0
        final_res << convert_three_digit(set)
      elsif idx == 3
        final_res << "billion" unless set.to_i == 0
        final_res << convert_three_digit(set)
      elsif idx == 4
        final_res << "trillion"
        final_res << convert_three_digit(set)
      end
    end
    final_res.delete([])
    final_res.reverse.join(" ")
  end

   def convert_three_digit (str)
    hash_num = {"0" => "zero",
      "1" => "one",
      "2" => "two",
      "3" => "three",
      "4" => "four",
      "5" => "five",
      "6" => "six",
      "7" => "seven",
      "8" => "eight",
      "9" => "nine",
      "10" => "ten",
      "11" => "eleven",
      "12" => "twelve",
      "13" => "thirteen",
      "14" => "fourteen",
      "15" => "fifteen",
      "16" => "sixteen",
      "17" => "seventeen",
      "18" => "eighteen",
      "19" => "nineteen",
      "20" => "twenty",
      "30" => "thirty",
      "40" => "forty",
      "50" => "fifty",
      "60" => "sixty",
      "70" => "seventy",
      "80" => "eighty",
      "90" => "ninety"
    }
    res = []
    length = str.length

    case length
    when 1
      res << hash_num[str]
    when 2
      if str[0] == "1"
        res << hash_num[str]
      else
        res << hash_num[str[0]+"0"]
        res << hash_num[str[1]] unless str[1] == "0"
      end
    when 3
      if str[0..1] == "00"
        res << hash_num[str[2]] unless str[2] == "0"
      elsif str[0] == "0" && str[1] == "1"
        res << hash_num[str[1..2]]
      elsif str[0] == "0" && str[1] != "1"
        res << hash_num[str[1]+"0"]
        res << hash_num[str[2]] unless str[2] == "0"
      elsif str[0] != "0" && str[1] == "1"
        res << hash_num[str[0]]
        res << "hundred"
        res << hash_num[str[1..2]]
      elsif str[0] != "0" && str[1] != "1"
        res << hash_num[str[0]]
        res << "hundred"
        res << hash_num[str[1]+"0"] unless str[1] == "0"
        res << hash_num[str[2]] unless str[2] == "0"
      end
    end
    res
  end
end
